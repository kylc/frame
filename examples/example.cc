#include <iostream>
#include <Eigen/Core>

#include "frame/frame.h"
#include "frame/frame_cache.h"
#include "frame/frame_point.h"
#include "frame/math_types.h"
#include "frame/twist.h"
#include "frame/wrench.h"

using namespace frame;
using namespace Eigen;

int main(int argc, char **argv) {
    Frame frame1;
    Frame frame2;

    FrameCache cache;
    cache.Insert(frame1, Isometry3d(), Twist());
    // frame2 is translated vertically 1 unit from frame1
    cache.Insert(frame2, Isometry3d(Translation3d(0.0, 0.0, 1.0)), Twist());

    FramePoint p1(frame1);
    std::cout << "p1 frame=" << p1.frame().unique_id() << " position=\n" << p1.position() << std::endl;

    p1.Transform(frame2, cache);
    std::cout << "p1 frame=" << p1.frame().unique_id() << " position=\n" << p1.position() << std::endl;

    return 0;
}
