#ifndef FRAME_TWIST_H_
#define FRAME_TWIST_H_

#include <Eigen/Dense>

namespace frame {

class Twist {
 public:
  Twist() {}
  Twist(Eigen::Vector3d linear, Eigen::Vector3d angular)
      : linear_(linear), angular_(angular) {}
  Twist(const Twist& rhs) = default;
  Twist& operator=(const Twist& rhs) = default;

  const Eigen::Vector3d linear() const { return linear_; }
  const Eigen::Vector3d angular() const { return angular_; }

 private:
  Eigen::Vector3d linear_;
  Eigen::Vector3d angular_;
};

};  // namespace frame

#endif  // FRAME_TWIST_H_
