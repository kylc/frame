#ifndef FRAME_FRAME_H_
#define FRAME_FRAME_H_

#include <atomic>
#include <climits>
#include <stdexcept>

namespace frame {

// An identifier for a coordinate frame.
//
// Rather than directly storing coordinate frame transform and velocity
// information, this class merely serves as an identifier to be used to
// reference and share frames.
//
// A frame is uniquely identified by its unique ID. No guarantees are made about
// how this ID is generated, except that it will never be shared between two
// constructed frames. Copied frames will share IDs.
class Frame {
 public:
  // Create a new frame with a unique ID.
  Frame() : unique_id_(current_unique_id_.fetch_add(1)) {
    if (unique_id() == ULONG_MAX) {
      throw std::overflow_error("too many frames allocated");
    }
  }

  // Copy a frame, reusing its ID.
  Frame(const Frame& rhs) = default;

  // Assign the provided frame's ID to this frame.
  Frame& operator=(const Frame& rhs) = default;

  unsigned long unique_id() const { return unique_id_; }

  bool operator==(const Frame& rhs) const {
    return unique_id_ == rhs.unique_id_;
  }

 private:
  // The internal counter that ensures no two frames are ever created with the
  // same ID. An atomic is used to ensure thread-safety.
  static std::atomic_ulong current_unique_id_;

  unsigned long unique_id_;
};

};  // namespace frame

namespace std {
template <>
struct hash<frame::Frame> {
  size_t operator()(const frame::Frame& f) const {
    // Defer hash to the Frame's single member variable -- it's ID.
    return hash<unsigned long>()(f.unique_id());
  }
};
}  // namespace std

#endif  // FRAME_FRAME_H_
