#ifndef FRAME_FRAME_POINT_H_
#define FRAME_FRAME_POINT_H_

#include "Eigen/Dense"
#include "frame/frame.h"
#include "frame/frame_cache.h"

namespace frame {

class FramePoint {
 public:
  FramePoint(Frame frame) : frame_(frame) {}
  FramePoint(Frame frame, Eigen::Vector3d position)
      : frame_(frame), position_(position) {}
  FramePoint(const FramePoint& rhs) = default;
  FramePoint& operator=(const FramePoint& rhs) = default;

  void Transform(const Frame& to_frame, const FrameCache& cache) {
    // First, transform the position to the world frame.
    position_ = cache.TransformToWorld(frame_) * position_;

    // Now apply the inverse of the destination frame's world transform to
    // transform into the destionation frame.
    position_ = cache.TransformToWorld(to_frame).inverse() * position_;

    frame_ = to_frame;
  }

  Frame frame() const { return frame_; }

  const Eigen::Vector3d& position() const { return position_; }

 private:
  Frame frame_;
  Eigen::Vector3d position_;
};

};  // namespace frame

#endif  // FRAME_FRAME_POINT_H_
