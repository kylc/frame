#ifndef FRAME_FRAME_CACHE_H_
#define FRAME_FRAME_CACHE_H_

#include <unordered_map>

#include <Eigen/Dense>
#include "frame/frame.h"
#include "frame/math_types.h"
#include "frame/twist.h"

namespace frame {

struct FrameCacheElement {
  Eigen::Isometry3d transform_to_world;
  Twist spatial_velocity_wrt_world;
};

class FrameCache {
 public:
  FrameCache() = default;
  FrameCache(const FrameCache& rhs) = default;
  FrameCache& operator=(const FrameCache& rhs) = default;

  void Insert(const Frame& frame, const Eigen::Isometry3d& transform_to_world,
              const Twist& velocity_wrt_world) {
    FrameCacheElement el{transform_to_world, velocity_wrt_world};
    elements_[frame] = el;
  }

  const Eigen::Isometry3d& TransformToWorld(const Frame& frame) const {
    return elements_.at(frame).transform_to_world;
  }

  const Twist& SpatialVelocityWrtWorld(const Frame& frame) const {
    return elements_.at(frame).spatial_velocity_wrt_world;
  }

 private:
  std::unordered_map<Frame, FrameCacheElement> elements_;
};

};  // namespace frame

#endif  // FRAME_FRAME_CACHE_H_
