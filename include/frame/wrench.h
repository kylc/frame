#ifndef FRAME_WRENCH_H_
#define FRAME_WRENCH_H_

#include <Eigen/Core>

namespace frame {

class Wrench {
 public:
  Wrench() {}
  Wrench(Eigen::Vector3d linear, Eigen::Vector3d angular)
      : linear_(linear), angular_(angular) {}
  Wrench(const Wrench& rhs) = default;
  Wrench& operator=(const Wrench& rhs) = default;

  const Eigen::Vector3d linear() const { return linear_; }

  const Eigen::Vector3d angular() const { return angular_; }

 private:
  Eigen::Vector3d linear_;
  Eigen::Vector3d angular_;
};

};  // namespace frame

#endif  // FRAME_WRENCH_H_
