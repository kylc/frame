#ifndef FRAME_FRAME_TWIST_H_
#define FRAME_FRAME_TWIST_H_

#include <Eigen/Dense>
#include "frame/frame.h"
#include "frame/frame_cache.h"
#include "frame/twist.h"

namespace frame {

class FrameTwist {
 public:
  FrameTwist(Frame body_frame, Frame wrt_frame, Frame expressed_frame)
      : body_frame_(body_frame),
        wrt_frame_(wrt_frame),
        expressed_frame_(expressed_frame) {}

  FrameTwist(Frame body_frame, Frame wrt_frame, Frame expressed_frame,
             const Eigen::Vector3d& linear, const Eigen::Vector3d& angular)
      : body_frame_(body_frame),
        wrt_frame_(wrt_frame),
        expressed_frame_(expressed_frame),
        twist_(linear, angular) {}

  FrameTwist(Frame body_frame, Frame wrt_frame, Frame expressed_frame,
             const Twist& twist)
      : body_frame_(body_frame),
        wrt_frame_(wrt_frame),
        expressed_frame_(expressed_frame),
        twist_(twist) {}

  FrameTwist(const FrameTwist& rhs) = default;
  FrameTwist& operator=(const FrameTwist& rhs) = default;

  Frame body_frame() const { return body_frame_; }
  Frame wrt_frame() const { return wrt_frame_; }
  Frame expressed_frame() const { return expressed_frame_; }

  const Eigen::Vector3d linear() const { return twist_.linear(); }
  const Eigen::Vector3d angular() const { return twist_.angular(); }

 private:
  Frame body_frame_;
  Frame wrt_frame_;
  Frame expressed_frame_;
  Twist twist_;
};

};  // namespace frame

#endif  // FRAME_FRAME_TWIST_H_
